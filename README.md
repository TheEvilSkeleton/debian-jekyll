# Debian Jekyll toolbox

**This project is deprecated! It has been moved to https://framagit.org/TheEvilSkeleton/jekyll-toolbox.**

## Prerequisites:

- `toolbox`
- `podman`

## Installation

```bash
curl -L https://framagit.org/TheEvilSkeleton/debian-jekyll/-/raw/master/install.sh | bash
```

## Setting up

### bash

```bash
alias bundle="toolbox run -c debian-jekyll bundle"
alias jekyll="toolbox run -c debian-jekyll jekyll"
alias gem="toolbox run -c debian-jekyll gem"
```

### fish

```fish
alias bundle "toolbox run -c debian-jekyll bundle"
alias jekyll "toolbox run -c debian-jekyll jekyll"
alias gem "toolbox run -c debian-jekyll gem"
```

## Uninstallation

```bash
podman kill debian-jekyll
toolbox rm debian-jekyll
```
