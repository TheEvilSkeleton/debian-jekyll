#!/usr/bin/env bash

curl https://framagit.org/TheEvilSkeleton/debian-jekyll/-/raw/master/debian-jekyll-toolbox.Dockerfile -o /tmp/debian-jekyll-toolbox.Dockerfile

podman build -t debian-jekyll-toolbox -f /tmp/debian-jekyll-toolbox.Dockerfile /tmp
toolbox create -c debian-jekyll --image debian-jekyll-toolbox
